# Functors

[![travis status](https://travis-ci.org/KeitaNakamura/Functors.jl.svg?branch=master)](https://travis-ci.org/KeitaNakamura/Functors.jl)
[![appveyor status](https://ci.appveyor.com/api/projects/status/hjn0u6aellja8r6q/branch/master?svg=true)](https://ci.appveyor.com/project/KeitaNakamura/functors-jl/branch/master)

[![coveralls status](https://img.shields.io/coveralls/github/KeitaNakamura/Functors.jl/master.svg?label)](https://coveralls.io/github/KeitaNakamura/Functors.jl?branch=master)
[![codecov status](http://codecov.io/github/KeitaNakamura/Functors.jl/coverage.svg?branch=master)](http://codecov.io/github/KeitaNakamura/Functors.jl?branch=master)
