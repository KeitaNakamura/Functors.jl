using Functors
using Base.Test

@testset "@functor" begin
    for T in (Float32, Float64)
        @testset "no parameter" begin
            @functor function not_anonymous(a::Real, b::Real)
                a + b
            end
            @test method_exists(not_anonymous, (Functors.Functor, Real, Real))
            f = @functor function(a::Real, b::Real)
                a + b
            end
            @test fieldtype(typeof(f), :params) == Tuple{}
            @test (@inferred f(   2,    3))::Int == 5
            @test (@inferred f(T(2), T(3)))::T == 5
            @test_throws ArgumentError f[Val{:c}]
            # anonymous
            g = @functor (a::Real, b::Real) -> a + b
            @test fieldtype(typeof(g), :params) == Tuple{}
            @test (@inferred g(   2,    3))::Int == 5
            @test (@inferred g(T(2), T(3)))::T == 5
            @test_throws ArgumentError g[Val{:c}]
        end
        @testset "parameter without type annotation" begin
            c = rand(T)
            f = @functor function(a::Real, b::Real; c)
                a + b + c
            end
            @test fieldtype(typeof(f), :params) == Tuple{T}
            @test (@inferred f(   2,    3))::T == 5 + c
            @test (@inferred f(T(2), T(3)))::T == 5 + c
            @test (@inferred f[Val{:c}])::T == c
            # anonymous
            g = @functor (a::Real, b::Real; c) -> a + b + c
            @test fieldtype(typeof(g), :params) == Tuple{T}
            @test (@inferred g(   2,    3))::T == 5 + c
            @test (@inferred g(T(2), T(3)))::T == 5 + c
            @test (@inferred g[Val{:c}])::T == c
        end
        @testset "parameter with type annotation" begin
            c = rand(T)
            f = @functor function(a::Real, b::Real; c::T = c)
                a + b + c
            end
            @test fieldtype(typeof(f), :params) == Tuple{T}
            @test (@inferred f(   2,    3))::T == 5 + c
            @test (@inferred f(T(2), T(3)))::T == 5 + c
            @test (@inferred f[Val{:c}])::T == c
            # anonymous
            g = @functor (a::Real, b::Real; c::T = c) -> a + b + c
            @test fieldtype(typeof(g), :params) == Tuple{T}
            @test (@inferred g(   2,    3))::T == 5 + c
            @test (@inferred g(T(2), T(3)))::T == 5 + c
            @test (@inferred g[Val{:c}])::T == c
        end
        @testset "errors" begin
            c = rand(T)
            @test_throws TypeError @functor function(a::Real, b::Real; c::Int = c)
                a + b + c
            end
            @test_throws TypeError @functor function(a::Real, b::Real; c::Int)
                a + b + c
            end
            # anonymous
            @test_throws TypeError @functor (a::Real, b::Real; c::Int = c) -> a + b + c
            @test_throws TypeError @functor (a::Real, b::Real; c::Int) -> a + b + c
        end
    end
end
