__precompile__()

module Functors

using Base: @_inline_meta
using MacroTools

export Functor, @functor

struct Functor{pnames <: Tuple, F, T <: Tuple}
    f::F
    params::T
end
@inline Functor{pnames}(f::F, params::T) where {pnames, F, T} = Functor{pnames, F, T}(f, params)

@inline (this::Functor)(args...) = this.f(this, args...)

@generated function Base.getindex(f::Functor{pnames}, ::Type{Val{name}}) where {pnames, name}
    i = findfirst(x -> x == name, pnames.parameters)
    i == 0 && return throw(ArgumentError("Functor has no parameter $name."))
    return quote
        @_inline_meta
        @inbounds return f.params[$i]
    end
end

macro functor(expr::Expr)
    if isdef(expr)
        def = splitdef(expr)
    else
        def = splitdef(Expr(:function, Expr(:call, :anonymous, expr.args[1].args...), expr.args[2]))
    end
    pnames = []
    for (i, kw) in enumerate(def[:kwargs])
        if isa(kw, Symbol) || Meta.isexpr(kw, :(::))
            var, val = kw, kw
        elseif Meta.isexpr(kw, :kw)
            var, val = kw.args
        else
            throw(ArgumentError("expression $kw is not supported in @fanctor."))
        end
        if Meta.isexpr(var, :(::))
            val = Expr(:(::), val, var.args[2])
            var = var.args[1]
        end
        def[:kwargs][i] = val
        push!(pnames, var)
    end
    params = Expr(:tuple, def[:kwargs]...)
    def[:args] = [[:__this__]; def[:args]]
    def[:kwargs] = []
    def[:body] = Expr(:let, def[:body], [:($var = __this__[$(Val{var})]) for var in pnames]...)
    f = def[:name] == :anonymous ? combinedef_anonymous(def) : MacroTools.combinedef(def)
    return esc(:($Functor{$(Tuple{pnames...})}($f, $params)))
end

function combinedef_anonymous(def::Dict{Symbol, Any})
    rtype = get(def, :rtype, :Any)
    all_params = [get(def, :params, [])..., get(def, :whereparams, [])...]
    return :(function ($(def[:args]...),) where {$(all_params...),}
                 convert($rtype, $(def[:body]))
             end)
end

end # module
